<?php
/**
 * @file
 * A module allowing drupal to use different Logos for different domains.
 */

/**
 * Implements hook_help().
 */
function domain_individual_logo_help($path, $arg) {
  if ($path == 'admin/help#domain-individual-logo') {
    return t('Load different Logo files for different domains.');
  }
}

/**
 * Implements hook_menu().
 */
function domain_individual_logo_menu() {
  $items = array();

  // Display a "Domain Logo" tab when viewing/editing a domain.
  $items['admin/structure/domain/view/%/domain_individual_logo'] = array(
    'title' => 'Domain Logo',
    'description' => 'Configuration for the Mondula Domain Logo module.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('domain_individual_logo_form', 4),
    'access arguments' => array('administer domains'),
    'type' => MENU_LOCAL_TASK,
  );

  return $items;
}

/**
 * Form builder; Create and display the User Warn configuration settings form.
 */
function domain_individual_logo_form($form, &$form_state, $domain_id) {
  // Just makes the form a little pretier.
  $form['group'] = array(
    '#type' => 'fieldset',
    '#title' => t('Domain specific Logo settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  // Load previous config for the domain in question.
  $query = db_select('domain_individual_logo', 'm');
  $query->fields('m', array('logo_file'));
  $query->condition('m.domain_id', $domain_id);
  $result = $query->execute();

  $default_file = ($result->rowCount() == 0) ? '' : $result->fetchField();

  // Text field for logo file name input.
  $form['group']['domain_individual_logo_file'] = array(
    '#type' => 'textfield',
    '#title' => t('Logo File'),
    '#description' => t('Enter a Logo file name located in sites/all/themes/yourtheme/'),
    '#size' => 60,
    '#maxlength' => 150,
    '#required' => TRUE,
    '#default_value' => $default_file,
  );

  // Submit button.
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
  );

  return $form;
}

/**
 * Implements hook_form_FORM_ID_submit().
 */
function domain_individual_logo_form_submit($form, &$form_state) {
  // Aquire submitted form data.
  $domain_id = $form_state['build_info']['args'][0];
  $file = $form_state['values']['domain_individual_logo_file'];

  // Query database for previous data.
  $query_check_for_entry = "SELECT * FROM {domain_individual_logo} WHERE domain_id = :domain_id";
  $result = db_query($query_check_for_entry, array(':domain_id' => $domain_id));

  // If no previous config is found.
  if ($result->rowCount() == 0) {
    // Insert a new row into the database.
    db_insert('domain_individual_logo')
      ->fields(array(
        'domain_id' => $domain_id,
        'logo_file' => $file,
      ))
      ->execute();
  }
  else {
    // Run an update of the existing row otherwise.
    db_update('domain_individual_logo')
      ->condition('domain_id', $domain_id)
      ->fields(array('logo_file' => $file))
      ->execute();
  }

  drupal_set_message(t('The settings have been saved'));
}

/**
 * Implements hook_preprocess_page().
 */
function domain_individual_logo_preprocess_page(&$variables) {
  // Needed globals.
  global $_domain, $theme, $base_url;
  // Current domain id.
  $domain_id = $_domain['domain_id'];

  // Query database for configuration.
  $query = db_select('domain_individual_logo', 'm');
  $query->fields('m', array('logo_file'));
  $query->condition('m.domain_id', $domain_id);
  $result = $query->execute();

  $file = drupal_get_path('theme', $theme) . '/' . $result->fetchField();

  // If a configuration is found, load logo file before page is rendered.
  if ($result->rowCount() >= 0) {
    $variables['logo'] = $base_url . $file;
  }
}
